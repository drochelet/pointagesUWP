﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

namespace pointages
{

    public sealed partial class insertionPage : Page
    {
        public insertionPage()
        {
            this.InitializeComponent();
        }

        private void _boutonOK_Click(object sender, RoutedEventArgs e)
        {
            TimeSpan duree = _fin.Time - _debut.Time;
            sqliteWrapper.writeToBase(_journee.Date, duree, _client.Text, _projet.Text, _ticket.Text);
        }

        private void _boutonVider_Click(object sender, RoutedEventArgs e)
        {

        }

        private void _client_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void _projet_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void _Pointe_Tapped(object sender, TappedRoutedEventArgs e)
        {

        }
    }
}
